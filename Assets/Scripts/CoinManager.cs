using Components.Health;
using UnityEngine;

public class CoinManager: MonoBehaviour
{
    private int _coins;
    
    public event Notify OnCoinsChanged;
    
    private void Awake()
    {
        _coins = 0;
    }

    public int GetCoins()
    {
        return _coins;
    }

    public void SetCoin(int amount)
    {
        if (_coins + amount <= -1)
        {
            return;
        }

        _coins += amount;
        OnCoinsChanged?.Invoke();
    }
}