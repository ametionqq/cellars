using UnityEngine;

namespace Components
{
    public class BulletComponent: MonoBehaviour
    {
        private CoinManager _coinManager;

        private void Awake()
        {
            _coinManager = FindFirstObjectByType<CoinManager>();
        }

        public void AddCoin()
        {
            _coinManager.SetCoin(5);
        }
    }
}