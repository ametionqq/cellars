using System;
using UnityEngine;

namespace Components
{
    public class DestroyComponent : MonoBehaviour
    {
        [SerializeField] private bool deleteByTime;
        [SerializeField] private float timeForDestroy;

        private void Start()
        {
            if (deleteByTime)
            {
                Destroy(gameObject, timeForDestroy);
            }
        }

        public void Destroy()
        {
            Destroy(gameObject);
        }
        
        public void DestroyChosen(GameObject target)
        {
            Destroy(target);
        }
    }
}