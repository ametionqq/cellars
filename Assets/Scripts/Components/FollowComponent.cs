using System;
using UnityEngine;

namespace Components
{
    public class FollowComponent : MonoBehaviour
    {
        [SerializeField] private float moveSpeed;
        [SerializeField] private string followingTag;
        [SerializeField] private bool isMoving;
        
        private GameObject _target;
        private Rigidbody2D _rb;

        private void Awake()
        {
            _rb = GetComponent<Rigidbody2D>();
            _target = GameObject.FindGameObjectWithTag(followingTag);
        }

        private void Update()
        {
            try
            {
                if (isMoving)
                {
                    Vector2 direction = _target.transform.position - transform.position;

                    var angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
                    transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle - 90));

                    transform.position += (Vector3)direction * (Time.deltaTime * moveSpeed);
                }
            }
            catch (Exception ex)
            {
                // ignored
            }
        }

        private void FixedUpdate()
        {
            LookAtClosestEnemy();
        }

        // ReSharper disable Unity.PerformanceAnalysis
        private void LookAtClosestEnemy()
        {
            var enemies = GameObject.FindGameObjectsWithTag(followingTag);

            GameObject closestEnemy = null;
            var minDistance = Mathf.Infinity;

            foreach (var enemy in enemies)
            {
                var distance = Vector2.Distance(transform.position, enemy.transform.position);
            
                if (!(distance < minDistance)) continue;
            
                closestEnemy = enemy;
                minDistance = distance;
            }

            if (closestEnemy == null) return;
        
            Vector2 direction = closestEnemy.transform.position - transform.position;
        
            var angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg + 90;
            var rotation = Quaternion.Euler(0, 0, angle - 90);
            transform.rotation = rotation;

        }

    }
}