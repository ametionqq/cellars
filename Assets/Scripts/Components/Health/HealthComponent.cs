using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace Components.Health
{
    public delegate void Notify();
    
    public class HealthComponent: MonoBehaviour
    {
        [SerializeField] private int health;
        [SerializeField] private int maxHealth;
        [SerializeField] private UnityEvent onDeath;

        public event Notify OnHealthChanged;
        
        public int GetHealth()
        {
            return health;
        }
    
        public void ModifyHealth(int delta)
        {
            if (health + delta > maxHealth)
            {
                return;
            }
            
            health += delta;
            OnHealthChanged?.Invoke();
            
            if (health <= 0)
            {
                onDeath?.Invoke();
            }
        }
    }
}