using UnityEngine;
using UnityEngine.SceneManagement;

namespace Components
{
    public class SceneComponent: MonoBehaviour
    {
        public void ReloadScene()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
        }
    }
}