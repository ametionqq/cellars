using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private GameObject projectilePrefab;
    [SerializeField] private Transform gun;
    [SerializeField] private PlayerAbilities playerAbilities;
    
    private Rigidbody2D _rb;
    private float _timeSinceSpawn;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
    }
    
    private void Update()
    {
        Shooting();
    }

    private void FixedUpdate()
    {
        var horizontal = Input.GetAxis("Horizontal");
        var vertical = Input.GetAxis("Vertical");

        var movement = new Vector2(horizontal, vertical).normalized;
    
        _rb.velocity = movement * playerAbilities.GetMoveSpeed();
    }
    
    private void Shooting()
    {
        if (projectilePrefab == null) return;
        
        _timeSinceSpawn += Time.deltaTime;
        if (!(_timeSinceSpawn > playerAbilities.GetShootSpeed())) return;
        
        Instantiate(projectilePrefab, gun.position, gun.rotation);
        
        _timeSinceSpawn = 0;
    }
}
