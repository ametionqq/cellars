using UnityEngine;

public class PlayerAbilities: MonoBehaviour
{
        private float _moveSpeed;
        private float _shootSpeed;
        private int _maxHp;

        private void OnEnable()
        {
                _moveSpeed = 5;
                _shootSpeed = 1;
                _maxHp = 3;
        }

        public float GetMoveSpeed()
        { 
                return _moveSpeed;
        }

        public void SetMoveSpeed(float newMoveSpeed)
        {
                if (newMoveSpeed <= 0)
                {
                        return;
                }

                _moveSpeed += newMoveSpeed;
        }
        
        public float GetShootSpeed()
        { 
                return _shootSpeed;
        }

        public void SetShootSpeed(float newShootSpeed)
        {
                if (newShootSpeed <= 0)
                {
                        return;
                }

                _shootSpeed = newShootSpeed;
        }

        public int GetMaxHp()
        {
                return _maxHp;
        }

        public void SetMaxHp(int newMaxHp)
        {
                if (newMaxHp <= 0)
                {
                        return;
                }

                _shootSpeed = newMaxHp;
        }
}