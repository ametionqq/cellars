using UI;
using UnityEngine;
using UnityEngine.Serialization;

public class SceneManager : MonoBehaviour
{
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private GameObject healPrefab;
    [SerializeField] private float spawnEnemyInterval = 5f;
    [SerializeField] private float spawnHealInterval = 30f;
    [SerializeField] private float spawnDistance = 10f;
    [SerializeField] private Transform playerTransform;
    [SerializeField] private UIControllerMainLevel uiControllerMainLevel;

    private float _timeSinceEnemySpawn;
    private float _timeSinceHealSpawn;

    private void Update()
    {
        HandleInput();
        HandleEnemySpawning();
    }

    private void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            uiControllerMainLevel.ToggleWindow("shop");
        }
    }

    private void HandleEnemySpawning()
    {
        _timeSinceEnemySpawn += Time.deltaTime;
        if (_timeSinceEnemySpawn > spawnEnemyInterval)
        {
            SpawnEnemy();
            _timeSinceEnemySpawn = 0;
        }
        
        _timeSinceHealSpawn += Time.deltaTime;
        if (_timeSinceHealSpawn > spawnHealInterval)
        {
            SpawnHeal();
            _timeSinceHealSpawn = 0;
        }
    }

    private void SpawnEnemy()
    {
        var randomDirection = Random.insideUnitCircle.normalized;
        var spawnPosition = randomDirection * spawnDistance + (Vector2)playerTransform.position;

        spawnPosition.y = Mathf.Clamp(spawnPosition.y, -23, 23);
        spawnPosition.x = Mathf.Clamp(spawnPosition.x, -39, 39);

        if (!Physics2D.OverlapCircle(spawnPosition, 0.5f))
        {
            Instantiate(enemyPrefab, spawnPosition, Quaternion.identity);
        }
    }
    
    private void SpawnHeal()
    {
        var randomDirection = Random.insideUnitCircle.normalized;
        var spawnPosition = randomDirection * spawnDistance + (Vector2)playerTransform.position;

        spawnPosition.y = Mathf.Clamp(spawnPosition.y, -25, 25);
        spawnPosition.x = Mathf.Clamp(spawnPosition.x, -39, 39);

        if (!Physics2D.OverlapCircle(spawnPosition, 0.5f))
        {
            Instantiate(healPrefab, spawnPosition, Quaternion.identity);
        }
    }

}