using UnityEngine;

public class ShopManager: MonoBehaviour
{
    [SerializeField] private PlayerAbilities playerAbilities;
    [SerializeField] private CoinManager coinManager;

    private int _moveSpeedLevel;
    private int _shootSpeedLevel;
    private int _maxHp;

    private void Awake()
    {
        _moveSpeedLevel = 1;
        _shootSpeedLevel = 1;
        _maxHp = 3;
    }

    public int GetMoveSpeedLevel()
    {
        return _moveSpeedLevel;
    }
    
    public int GetShootSpeedLevel()
    {
        return _shootSpeedLevel;
    }
    
    public void UpgradeMoveSpeed()
    {
        if (_moveSpeedLevel <= 2)
        {
            if (coinManager.GetCoins() - 10 < 0) return;
                
            coinManager.SetCoin(-10);
            playerAbilities.SetMoveSpeed(3);
            _moveSpeedLevel++;
        }
    }

    public void UpgradeShootSpeed()
    {
        if (_shootSpeedLevel <= 2)
        {
            if (!(playerAbilities.GetShootSpeed() >= 0.1f)) return;
            if (coinManager.GetCoins() - 25 < 0) return;

            coinManager.SetCoin(-25);

            playerAbilities.SetShootSpeed(playerAbilities.GetShootSpeed() * 0.9f);
            _shootSpeedLevel++;
        }
    }
}