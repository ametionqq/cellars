using Components.Health;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UIElements;

namespace UI
{
    public class UIControllerMainLevel : MonoBehaviour
    {
        [SerializeField] private HealthComponent healthComponent;
        [SerializeField] private CoinManager coinManager;
        [SerializeField] private ShopManager shopManager;
        
        private VisualElement _rootElement;
        private bool _isWindowVisible;

        private void Start()
        {
            _rootElement = GetComponent<UIDocument>().rootVisualElement;

            SetHp();
            SetCoins();
            
            healthComponent.OnHealthChanged += SetHp;
            coinManager.OnCoinsChanged += SetCoins;
            
            _rootElement.Q<Button>("moveSpeedButton").clicked += BuyMoveSpeedUpgrade;
            _rootElement.Q<Button>("shootSpeedButton").clicked += BuyShootSpeedUpgrade;
            
            BuyMoveSpeedUpgrade();
            BuyShootSpeedUpgrade();
            
            _rootElement.Q<Label>("moveSpeedLabel").text = $"Move Speed {shopManager.GetMoveSpeedLevel()} Level";
            _rootElement.Q<Label>("shootSpeedLabel").text = $"Shoot Speed {shopManager.GetShootSpeedLevel()} Level";
        }

        private void OnDisable()
        {
            healthComponent.OnHealthChanged -= SetHp;
            coinManager.OnCoinsChanged -= SetCoins;

            _rootElement.Q<Button>("moveSpeedButton").clicked -= BuyMoveSpeedUpgrade;
            _rootElement.Q<Button>("shootSpeedButton").clicked -= BuyShootSpeedUpgrade;
        }
        
        private void SetHp()
        {
            var currentHp = healthComponent.GetHealth();

            for (var i = 0; i < 3; i++)
            {
                var hp = _rootElement.Q<VisualElement>("hp" + i);
                if (hp != null)
                {
                    hp.style.display = i < currentHp ? DisplayStyle.Flex : DisplayStyle.None;
                }
            }
        }

        private void SetCoins()
        {
            var coins = coinManager.GetCoins();
            
            _rootElement.Q<Label>("coins").text = coins.ToString();
        }
        
        private void BuyMoveSpeedUpgrade()
        {
            shopManager.UpgradeMoveSpeed();

            _rootElement.Q<Label>("moveSpeedLabel").text = $"Move Speed {shopManager.GetMoveSpeedLevel()} Level";
        }

        private void BuyShootSpeedUpgrade()
        {
            shopManager.UpgradeShootSpeed();
            
            _rootElement.Q<Label>("shootSpeedLabel").text = $"Shoot Speed {shopManager.GetShootSpeedLevel()} Level";
        }

        public void ToggleWindow(string windowName)
        {
            var window = _rootElement.Q<VisualElement>(windowName);
            
            _isWindowVisible = !_isWindowVisible;
            window.style.display = _isWindowVisible ? DisplayStyle.Flex : DisplayStyle.None;
            
            Time.timeScale = Time.timeScale == 1.0f ? 0f : 1.0f;
        }

        
    }
}
