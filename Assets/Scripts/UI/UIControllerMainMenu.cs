using System;
using UnityEngine;
using UnityEngine.UIElements;

public class UIController:  MonoBehaviour
{
        private Button playButton;

        private void Start()
        { 
                var root = GetComponent<UIDocument>().rootVisualElement;

                playButton = root.Q<Button>("playbutton");
        }

        private void Update()
        {
                playButton.clicked += () =>
                {
                        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
                };
        }
}