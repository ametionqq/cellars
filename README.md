
# 🎮 2.5D Game on Unity

Welcome to the **2.5D Vampire Survivors-like Game** repository! This project is built using Unity and leverages the new UI system while retaining the old input system. The game employs a component-based architecture to ensure modular and maintainable code.

## Features

- **New UI System**: Enjoy the sleek and modern interface provided by Unity's new UI system.
- **Old Input System**: For those who prefer the classic input handling.
- **Component-Based Approach**: Enhancing code modularity and maintainability.
- **2.5D Graphics**: Dive into an engaging semi-3D world inspired by Vampire Survivors.

## Getting Started

1. **Clone the Repository**:
   ```bash
   git clone https://github.com/yourusername/your-repo-name.git
   ```

2. **Open in Unity**:
   - Make sure you have the latest version of Unity installed.
   - Open the project in Unity Hub and let it compile.

3. **Play the Game**:
   - Hit the Play button in the Unity Editor to start the game.
   - Use the keyboard and mouse to navigate through the game.

## Contributions

We welcome contributions! Feel free to open issues or submit pull requests to help improve the game.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

---

Happy coding and have fun playing! 🚀
